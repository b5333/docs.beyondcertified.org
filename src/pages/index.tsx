import React from "react";
import clsx from "clsx";
import Link from "@docusaurus/Link";
import useDocusaurusContext from "@docusaurus/useDocusaurusContext";
import Layout from "@theme/Layout";
import HomepageFeatures from "@site/src/components/HomepageFeatures";

import styles from "./index.module.css";

function HomepageHeader() {
  const { siteConfig } = useDocusaurusContext();
  return (
    <header className={clsx("hero hero--primary", styles.heroBanner)}>
      <div className="container">
        <h1 className="hero__title">{siteConfig.title}</h1>
        <p className="hero__subtitle">{siteConfig.tagline}</p>
      </div>
    </header>
  );
}

export default function Home(): JSX.Element {
  const { siteConfig } = useDocusaurusContext();
  return (
    <Layout
      title={`Hello from ${siteConfig.title}`}
      description="Open source and standards proposals"
    >
      <HomepageHeader />
      <main>
        <h2 className={styles.featured}>Current Projects</h2>
        <ul className={styles.buttons}>
          <li>
            <Link
              className="button button--secondary button--lg"
              to="/qr-code/reuse/"
            >
              Reuse QR
            </Link>
            <p>
              A simple QR code format for reusable containers to quickly get the
              tare weight or volume of a container.
            </p>
          </li>
          <li>
            <Link
              className="button button--secondary button--lg"
              to="/qr-code/uri/v1.0/"
            >
              URI QR
            </Link>
          </li>
        </ul>
      </main>
    </Layout>
  );
}
